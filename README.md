<div style="text-align: center;">
  <img src="https://academiahack.com/assets/logo_home_header-d62c29ebbba00e839f115a5de43c4b9ea8e565ecac98f70ff01dc9367682b271.svg" alt="Markdownify" width="150">
</div>

Standalone Migrations & ActiveRecord Template
=====
Template de proyecto con Standalone Migrations y ActiveRecord para montar proyectos demostrativos con estos gems.

Comandos generales
-----
- Instalación de gems: ```bundle install```
- Ejecución de migrations: ```rake db:migrate```
- Se puede ejecutar la aplicación ubicada en la carpeta ```lib/``` con el siguiente comando:
```ruby lib/main.rb```
- Si se muestra un error de permisos para ejecutar alguno de los códigos, el siguiente comando seguramente resuelve el error: ```chmod +x bin/console.rb```

Comandos de migrations
-----
- Crear un migration para crear una nueva tabla:
```
rake db:new_migration name=CreateTableName options="field1:datatype field2:datatype...etc."
```
- Crear un migration para agregar un campo a una tabla existente:
```
rake db:new_migration name=AddFieldToTableName options="field:datatype"
```
- Crear un migration para agregar una clave foránea a una tabla:
```
rake db:new_migration name=AddRefTable1ToTable2 options="table1:references"
```
- Ejecutar los migrations pendientes:
```
rake db:migrate
```
- Deshacer el último migration ejecutado:
```
rake db:rollback
```
